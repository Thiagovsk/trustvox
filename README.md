# README

Para subir a aplicação:

`docker-compose up -d`

Algumas explicações sobre o que fiz no desafio:

1- Utilizei Docker para construir o container com ruby e usei o docker-compose para desenvolvimento

2- Utilizei o mongoDB como banco de dados e mongoID

3- Utilizei relacionamentos entre company e complaint, além de indices do mongo.

4- Utilizei rspec e factory_bot para escrever os testes

5- Utilizei rails modo api

6- Utilizei Reek e Rubocop para analise estática de código

7- Utilizei postman para executar a api, vocês podem usar também: 

https://www.getpostman.com/collections/2337f69a2f6516489ffd

8- Utilizei algmas boas praticas da API, como a gem rack-attack para limitar requests e habilitei também o CORS

9- Utilizei o fast json API para serialização dos objetos

10- Utilizei uns scripts úteis na pasta script para rodar os comandos dentro dos containers

11- Utilizei a documentação do swagger, para acessar: http://localhost:3000/api-docs

Não utilizei o versionamento de api, mas também é uma boa pratica versionar a api, exemplo:

/api/v1/complaints

Para estratégias de deploy:

Já deixei o arquivo de configuração para rodar os containers no heroku, está no arquivo heroku.yml

Fonte: https://devcenter.heroku.com/articles/build-docker-images-heroku-yml

De qualquer forma, como a aplicação ja está toda em Dockerfile, poderia ser feito o deploy
em qualquer ferramenta de orquestração de container.

Para rodar os testes (precisa subir a aplicação com docker-compose)

`./scripts/dbundle exec rspec`

Para rodar o reek/rubocop

`./scripts/dbundle exec reek`
`./scripts/dbundle exec rubocop`
