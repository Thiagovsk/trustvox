# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  resources :complaints, only: %i[index create show]
  resources :companies, only: %i[index create show] do
    resources :complaints, only: [:index]
  end
end
