# frozen_string_literal: true

class Complaint
  include Mongoid::Document
  store_in collection: :complaints

  field :title, type: String
  field :description, type: String
  field :location, type: Hash

  belongs_to :company

  validates :title, presence: true

  index(company_id: 1)
  index('location.city': 1)

  def self.from_companies_city(params)
    city = params[:location].try(:[], :city)
    company_id = params[:company_id]
    return where(company_id: company_id) unless city

    where(company_id: company_id, 'location.city': city)
  end
end
