# frozen_string_literal: true

class Company
  include Mongoid::Document
  store_in collection: :companies

  field :name, type: String
  has_many :complaint

  validates :name, presence: true
end
