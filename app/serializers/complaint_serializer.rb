# frozen_string_literal: true

class ComplaintSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :description, :location
end
