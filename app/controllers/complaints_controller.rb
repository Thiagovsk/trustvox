# frozen_string_literal: true

class ComplaintsController < ApplicationController
  before_action :set_complaint, only: %i[show update destroy]

  # GET /complaints
  def index
    @complaints = Complaint.from_companies_city(complaint_params)

    render json: ::ComplaintSerializer.new(@complaints).serialized_json
  end

  # GET /complaints/1
  def show
    render json: @complaint
  end

  # POST /complaints
  def create
    @complaint = Complaint.new(complaint_params)

    if @complaint.save
      render json: ::ComplaintSerializer.new(@complaint).serialized_json, status: :created, location: @complaint
    else
      render json: @complaint.errors, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_complaint
    @complaint = Complaint.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def complaint_params
    params.permit(:title, :description, :company_id, location: {})
  end
end
