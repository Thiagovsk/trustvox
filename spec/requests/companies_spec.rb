# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Companies', type: :request do
  describe 'GET /companies' do
    it 'should response 200' do
      get companies_path
      expect(response).to have_http_status(200)
    end
  end
end
