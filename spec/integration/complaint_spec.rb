# frozen_string_literal: true

require 'swagger_helper'

describe 'Complaints API' do
  path '/companies/{id}/complaints' do
    get 'list of complaints from company city' do
      tags 'Complaints'
      produces 'application/json'
      parameter name: :id, in: :path, type: :string

      response '200', 'complaint found' do
        schema type: :object,
               properties: {
            title: { type: :string },
            description: { type: :string },
            location: { type: :hash },
            id: { type: :string }
          },
               required: ['id']

        let(:id) { FactoryBot.create(:company).id }
      end
    end
  end
end
