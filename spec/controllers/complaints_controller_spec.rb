# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ComplaintsController, type: :controller do
  let(:company_id) { FactoryBot.create(:company).id }
  let(:valid_attributes) do
    FactoryBot.build(:complaint).attributes.merge(company_id: company_id)
  end

  let(:invalid_attributes) do
    { title: nil }
  end

  let(:valid_session) { {} }

  describe 'GET #index' do
    it 'returns a success response' do
      Complaint.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      complaint = Complaint.create! valid_attributes
      get :show, params: { id: complaint.to_param }, session: valid_session
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Complaint' do
        expect do
          post :create, params: valid_attributes, session: valid_session
        end.to change(Complaint, :count).by(1)
      end

      it 'renders a JSON response with the new complaint' do
        post :create, params: valid_attributes, session: valid_session
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(complaint_url(Complaint.last))
      end
    end

    context 'with invalid params' do
      it 'renders a JSON response with errors for the new complaint' do
        post :create, params: { complaint: invalid_attributes }, session: valid_session
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end
end
