# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CompaniesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/companies').to route_to('companies#index')
    end

    it 'routes to #create' do
      expect(post: '/companies').to route_to('companies#create')
    end

    it 'routes to #show' do
      expect(get: '/companies/1').to route_to('companies#show', id: '1')
    end

    it 'routes complaints from companies' do
      expect(get: '/companies/1/complaints').to route_to('complaints#index', company_id: '1')
    end
  end
end
