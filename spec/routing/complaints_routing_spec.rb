# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ComplaintsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/complaints').to route_to('complaints#index')
    end

    it 'routes to #show' do
      expect(get: '/complaints/1').to route_to('complaints#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/complaints').to route_to('complaints#create')
    end

    it 'routes complaints from companies' do
      expect(get: '/companies/1/complaints').to route_to('complaints#index', company_id: '1')
    end
  end
end
