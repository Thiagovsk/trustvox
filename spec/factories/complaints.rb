# frozen_string_literal: true

FactoryBot.define do
  factory :complaint do
    title { 'comida ruim restaurante' }
    description { 'comida estava fria' }
    location { { city: 'brasilia' } }
  end
end
