# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Complaint, type: :model do
  subject { described_class.new(params).valid? }
  let(:company_id) { FactoryBot.create(:company).id }
  let(:params) do
    FactoryBot.build(:complaint).attributes.merge(company_id: company_id)
  end

  describe 'valid params' do
    it { is_expected.to be_truthy }
  end

  describe 'invalid params' do
    let(:params) { {} }
    it { is_expected.to be_falsey }
  end

  describe '.from_companies_city' do
    subject { described_class.from_companies_city(params) }
    context 'when params have city' do
      let(:params) { { company_id: company_id, location: { city: 'brasilia' } } }
      it { is_expected.to be_truthy }
    end

    context 'when params have only company_id' do
      let(:params) { { company_id: company_id } }
      it { is_expected.to be_truthy }
    end
  end
end
